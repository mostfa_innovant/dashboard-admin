import React, { useEffect, useState } from "react";

const Filter = ({
  setActiveGenre,
  activeGenre,
  setFiltered,
  partners,
  filtered,
}) => {
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");
  const [name, setName] = useState("");
  useEffect(() => {
    if (activeGenre === "all") {
      setFiltered(partners);

      return;
    }
    const filtered = partners.filter((partner) =>
      partner.status.includes(activeGenre)
    );
    setFiltered(filtered);
  }, [activeGenre]);
  useEffect(() => {
    if (setSearch !== "") {
      const filtered = partners.filter((partner) =>
        partner.user.fullname.toLowerCase().includes(search.toLowerCase())
      );
      setFiltered(filtered);

      return;
    }
  }, [search]);
  const handleChange = (e) => {
    setSearch(e.target.value);
  };
  const app = partners.filter((partner) => {
    return partner.status.includes("approved");
  });
  const pen = partners.filter((partner) => {
    return partner.status.includes("pending");
  });
  function changeFilter(e, option) {
    setActiveGenre(option);
    document.querySelectorAll(".filter-btn").forEach((item) => {
      item.classList.remove("selected-btn");
    });
    e.target.classList.add("selected-btn");
  }

  return (
    <div>
      <div className="container">
        <button
          className="appro filter-btn"
          onClick={(e) => changeFilter(e, "approved")}
        >
          {" "}
          Approved ({app.length})
        </button>
        <button
          className="rejec filter-btn"
          onClick={(e) => changeFilter(e, "pending")}
        >
          Pending ({pen.length})
        </button>
        <button
          className="all filter-btn"
          onClick={(e) => changeFilter(e, "all")}
        >
          All ({partners.length})
        </button>
      </div>
      <div className="searchbar">
        <input
          className="search"
          type="text"
          value={search}
          onChange={handleChange}
          placeholder="Search..."
        />
      </div>
      <style jsx>{`
        button {
          margin-right: 2rem;
          min-width: 85px;
          padding: 0.5rem 0.5rem;
          border: none;
          height: 30px;
          color: white;
          border-radius: 5px;
          dispaly: flex;
          align-items: center;
          justify-content: center;
          font-size: 13px;
          cursor: pointer;
          font-weight: 400;
        }
        button p {
          margin-bottom: 8px;
        }
        button:active {
          background-color: #eee;
          color: #373a3c;
        }
        button:focus {
          background-color: #1e88e5;
          outline-offset: 1px;
        }
        .appro {
          background-color: #21c1d6;
        }
        .rejec {
          background-color: #fc4b6c;
        }
        .all {
          background-color: #ffb22b;
        }
        .round {
          height: 10px;
          width: 10px;
          border-radius: 50%;
          color: blue;
        }

        .container {
          margin-left: 10px;
        }
        .selected-btn {
          background-color: #1e88e5;
          outline-offset: 1px;
        }
        input {
          width: 250px;
          margin-left: 1300px;
          height: 25px;
        }
      `}</style>
    </div>
  );
};

export default Filter;
