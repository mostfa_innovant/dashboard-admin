import React, { useEffect, useState } from "react";

const Logged = ({ users, setFiltered, filtered }) => {
  const [activegmail, setActivegmail] = useState(false);
  const [activeemail, setActiveemail] = useState(false);
  const [activeapple, setActiveapple] = useState(false);
  const [search, setSearch] = useState("");
  const filterUser = () => {
    if (activegmail == true && activeemail == false && activeapple == false) {
      const fgmail = users.filter(
        (user) => user.isLoggedInWithGmail == activegmail
      );
      setFiltered(fgmail);
    }
    if (activegmail == false && activeemail == true && activeapple == false) {
      const femail = users.filter(
        (user) => user.isLoggedInWithEmail == activeemail
      );
      setFiltered(femail);
    }
    if (activegmail == false && activeemail == false && activeapple == true) {
      const fapple = users.filter(
        (user) => user.isLoggedInWithIOS == activeapple
      );
      setFiltered(fapple);
    }
    if (activegmail == false && activeemail == false && activeapple == false) {
      setFiltered(users);
    }
  };
  useEffect(() => {
    filterUser();
  }, [activeapple, activeemail, activegmail]);
  useEffect(() => {
    if (setSearch !== "") {
      const filtered = users.filter((user) =>
        user.fullname.toLowerCase().includes(search.toLowerCase())
      );
      setFiltered(filtered);

      return;
    }
  }, [search]);
  const handleChange = (e) => {
    setSearch(e.target.value);
  };
  const email = users.filter((user) => {
    return user.isLoggedInWithEmail.toString().includes("true");
  });
  const apple = users.filter((user) => {
    return user.isLoggedInWithIOS.toString().includes("true");
  });
  const gmail = users.filter((user) => {
    return user.isLoggedInWithGmail.toString().includes("true");
  });

  return (
    <div>
      <div className="container">
        <button
          className="gmaillog"
          onClick={() => (
            setActivegmail(true), setActiveapple(false), setActiveemail(false)
          )}
        >
          {" "}
          google ({gmail.length})
        </button>
        <button
          className="applelog"
          onClick={() => (
            setActivegmail(false), setActiveapple(false), setActiveemail(true)
          )}
        >
          gmail ({email.length})
        </button>
        <button
          className="emaillog"
          onClick={() => (
            setActivegmail(false), setActiveapple(true), setActiveemail(false)
          )}
        >
          apple ({apple.length})
        </button>
        <button
          className="all"
          onClick={() => (
            setActivegmail(false), setActiveapple(false), setActiveemail(false)
          )}
        >
          all ({users.length})
        </button>
      </div>
      <div className="searchbar">
        <input
          className="search"
          type="text"
          value={search}
          onChange={handleChange}
          placeholder="Search..."
        />
      </div>
      <style jsx>{`
        button {
          margin-right: 2rem;
          min-width: 85px;
          padding: 0.5rem 0.5rem;
          border: none;
          height: 30px;
          color: white;
          border-radius: 5px;
          dispaly: flex;
          align-items: center;
          justify-content: center;
          font-size: 13px;
          cursor: pointer;
          font-weight: 400;
        }
        button p {
          margin-bottom: 8px;
        }
        button:active {
          background-color: #1e88e5;
          color: #1e88e5;
        }
        button:focus {
          background-color:#1e88e5;
          outline-offset: 1px;
        }
        button:visited{
          background-color:#1e88e5;
        }

        .container {
          margin-left: 10px;
        }

        input {
          width: 250px;
          margin-left: 1300px;
          height: 30px;
        }
      `}</style>
    </div>
  );
};

export default Logged;
