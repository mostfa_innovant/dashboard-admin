import axios from "axios";
import React, { useState } from "react";
import Link from "next/link";
import { API_URL } from "../../utils/consts";
import { useRouter } from "next/router";
const Content = ({ data, token, getAllPartners, setLoading }) => {
  const [approved, setApproved] = useState(false);
  const router = useRouter();
  const updateStatus = (e, id) => {
    e.preventDefault();
    if (e.target.value == "profile") {
      router.push(`/partners/${id}`);
    }
    let body = { status: `${e.target.value}` };

    const config = {
      method: "PUT",
      url: `${API_URL}/admins/partners/${id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: body,
    };

    setLoading(true);
    axios(config)
      .then((res) => {
        getAllPartners();
      })
      .catch((err) => {
        console.error("err", err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <div className="container-table">
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>name</th>
            <th>email</th>
            <th>status</th>
            <th>Edit Status</th>
          </tr>
        </thead>
        <tbody>
          {data.map((user, i) => (
            <>
              <tr key={user.user._id}>
                <td>{i + 1}</td>
                <td>
                  <Link href={`/partners/${user._id}`}>
                    {user.user.fullname}
                  </Link>
                </td>
                <td>{user.user.email}</td>
                <td>
                  {user.status == "approved" ? (
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <div className="roundapp"></div>
                      <span>approved</span>{" "}
                    </div>
                  ) : (
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <div className="roundrejec"></div>
                      <span>pending</span>{" "}
                    </div>
                  )}
                </td>

                <div className="box">
                  <select
                    onChange={(e) => {
                      updateStatus(e, user._id);
                    }}
                  >
                    <option selected disabled>
                      Action
                    </option>

                    <option value="pending">pending</option>
                    <option value="approved">approved</option>
                    <option value="profile">view profile</option>
                  </select>
                </div>
              </tr>
              <style jsx>{`
                .box {
                  height: 40px;
                }
                .box select {
                  background-color: #72a0c1;
                  color: white;
                  padding: 12px;
                  width: 120px;
                  height: 40px;
                  border: none;
                  font-size: 14px;
                  box-shadow: 0 5px 25px rgba(0, 0, 0, 0.2);
                  -webkit-appearance: button;
                  appearance: button;
                  outline: none;
                  border-raduis: 5px;
                  margin-top: 5px;
                  border-radius: 5px;
                }
                .roundapp {
                  height: 10px;
                  width: 10px;
                  border-radius: 50%;
                  background-color: #21c1d6;
                  margin-right: 6px;
                }

                .roundrejec {
                  height: 10px;
                  width: 10px;
                  border-radius: 50%;
                  background-color: #fc4b6c;
                  margin-right: 6px;
                }

                .box:hover::before {
                  color: #eee;
                  background-color: #eee;
                }

                .box select option {
                  padding: 8px 16px;
                  background-color: white;
                  font-weight: 400;
                  font-size: 16px;
                  font-family: Ppopins;
                  color: #67757c;
                }
              `}</style>
            </>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Content;
