import React, { useState } from "react";

const Content = ({
  data,
  token,
  getAllReclamations,
  setLoading,
  isAddModalOpen,
  isModalOpen,
  setIsAddModalOpen,
  reclamationId,
  setReclamationId,
}) => {
  const handleCloseAddModal = () => {
    setIsAddModalOpen(false);
  };
  const onclick = (reclamationId) => {
    setIsAddModalOpen(true);
    setReclamationId(reclamationId);
  };

  return (
    <>
      <div className="container-table">
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>user</th>
              <th>book</th>
              <th>message</th>
            </tr>
          </thead>
          <tbody>
            {data.map((reclamation, i) => (
              <>
                <tr key={reclamation._id}>
                  <td>{i + 1}</td>
                  <td>{reclamation.user.fullname}</td>
                  <td>{reclamation.book.title}</td>
                  <td>
                    <button
                      className="display"
                      onClick={() => onclick(reclamation._id)}
                    >
                      Display
                    </button>
                  </td>
                </tr>

                <style jsx>{`
                  button {
                    margin-right: 2rem;
                    min-width: 85px;
                    padding: 0.5rem 0.5rem;
                    border: none;
                    height: 30px;
                    color: white;
                    border-radius: 5px;
                    dispaly: flex;
                    align-items: center;
                    justify-content: center;
                    font-size: 13px;
                    cursor: pointer;
                    font-weight: 400;
                  }
                  button p {
                    margin-bottom: 8px;
                  }
                `}</style>
              </>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Content;
