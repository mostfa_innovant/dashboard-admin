import { CardAnalytics } from "../widgets";

const Content = ({ zones }) => {
	if (Array.isArray(zones) && zones.length > 0) {
		return (
			<>
				<div className='grid'>
					{zones.map(({ _id, name, containersCount }) => (
						<CardAnalytics
							key={_id}
							icon='fas fa-trash'
							title={name}
							value={containersCount || 0}
							color='#44b6ae'
							link={`/zones/${_id}/containers`}
						/>
					))}
				</div>

				<style jsx>{`
					.grid {
						display: grid;
						grid-template-columns: repeat(4, 1fr);
						gap: 30px;
					}

					@media only screen and (max-width: 767px) {
						.grid {
							grid-template-columns: repeat(2, 1fr);
							gap: 10px;
						}
					}
				`}</style>
			</>
		);
	}

	return <></>;
};

export default Content;
