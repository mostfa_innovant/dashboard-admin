const KeyValue = ({ title, value }) =>
	value ? (
		<>
			<p>
				<span>{title}:</span> {value}
			</p>

			<style jsx>{`
				p {
					text-transform: capitalize;
					margin-bottom: 8px;
					font-size: 14px;
				}

				span {
					font-weight: bold;
					color: #578ebe;
				}
			`}</style>
		</>
	) : (
		<></>
	);

export default KeyValue;
