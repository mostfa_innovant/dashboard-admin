const BtnFilter = ({ title, number, color, isActive, handleClick }) => {
	return (
		<>
			<div className={isActive ? "btn-filter active" : "btn-filter"} onClick={handleClick}>
				<p>{title}</p>
				{(number || number === 0) && (
					<div className='tag'>
						<p>{number}</p>
					</div>
				)}
			</div>

			<style jsx>{`
				.btn-filter {
					padding: 6px 16px;
					border-radius: 3px;
					display: flex;
					align-items: center;
					gap: 8px;
					cursor: pointer;
					transition: all 300ms ease;
					border: 1px solid ${color};
				}

				.btn-filter:hover,
				.btn-filter.active {
					background-color: ${color};
				}

				.btn-filter:hover .tag,
				.btn-filter.active .tag {
					background-color: #fff;
				}

				.btn-filter:hover .tag p,
				.btn-filter.active .tag p {
					color: #000;
				}

				p {
					font-size: 14px;
					transition: all 300ms ease;
					white-space: nowrap;
				}

				.tag {
					padding: 2px 5px;
					border-radius: 50%;
					transition: all 300ms ease;
					background-color: ${color};
				}

				.tag p {
					font-size: 10px;
					color: #fff;
				}
			`}</style>
		</>
	);
};

export default BtnFilter;
