const Container = ({ children, maxWidth = "1700px" }) => {
  return (
    <>
      <div className="container">{children}</div>

      <style jsx>{`
        .container {
          margin: 0 auto;
          max-width: ${maxWidth};
        }
      `}</style>
    </>
  );
};

export default Container;
