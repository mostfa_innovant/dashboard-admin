import { useState, useEffect } from "react";
import axios from "axios";
import { markInputAsFalse, markInputAsTrue } from "../../utils/inputHandlers";
import { Modal } from "../widgets";
import { API_URL } from "../../utils/consts";
const UpdateCategoryModal = ({
  token,
  isModalOpen,
  getAllCategories,
  handleClose,
}) => {
  const [title, setTitle] = useState("");
  const [icon, setIcon] = useState("");
  const [image, setImage] = useState("");

  const handleAddCategory = () => {
    var formData = new FormData();
    formData.append("title", title);
    formData.append("icon", icon);
    formData.append("image", image);

    const config = {
      method: "PUT",
      url: `${API_URL}/admins/categories/${categoryId}`,

      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "multipart/form-data",
      },
      data: formData,
    };

    axios(config)
      .then(({ status }) => {
        if (status === 200) {
          console.log("status :>> ", status);
          getAllCategories();
          clearAndCloseModal();
        }
      })
      .catch((err) => {
        if (err.response) {
          const { status, data } = err.response;
          if (status === 400 || status === 401) {
            markInputAsFalse("form", data.error);
          }
        }
      });
  };

  const handleInputChange = (e, fn) => {
    fn(e.target.value);
    // markInputAsTrue(e.target.id);
  };
  const handleImageChange = (e, fn) => {
    fn(e.target.files[0]);
    // markInputAsTrue(e.target.id);
  };

  const isValid = () => {
    if (!title) {
      markInputAsFalse("category-title", "title is required");
      return false;
    }

    return true;
  };

  const clearAndCloseModal = () => {
    setTitle("");
    setIcon("");
    setImage("");
    handleClose();
  };

  return (
    <>
      <Modal
        title="Add Category"
        isModalOpen={isModalOpen}
        handleClose={clearAndCloseModal}
        footer={<button onClick={handleAddCategory}>Add Category</button>}
      >
        <form id="form">
          <input
            id="category-title"
            type="text"
            placeholder="title"
            value={title}
            onChange={(e) => handleInputChange(e, setTitle)}
          />
          <input
            type="file"
            id="category-icon"
            placeholder="icon"
            onChange={(e) => handleImageChange(e, setIcon)}
          />
          <input
            type="file"
            id="category-image"
            placeholder="Image"
            onChange={(e) => handleImageChange(e, setImage)}
          />
        </form>
      </Modal>
    </>
  );
};

export default UpdateCategoryModal;
