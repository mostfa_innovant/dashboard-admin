import Image from "next/image";
import { UPLOAD_URL } from "../../utils/consts";
const Content = ({ data, setIsUpdateModalOpen, setCategoryIdToUpdate }) => {
  const handleCloseAddModal = () => {
    setIsUpdateModalOpen(false);
  };
  const updateCategory = (updatId) => {
    setIsUpdateModalOpen(true);
    setCategoryIdToUpdate(updatId);
  };

  return (
    <div className="container-table">
      <table>
        <thead>
          <tr>
            <th>#</th>

            <th>icon</th>
            <th>image</th>
            <th>title</th>
            <th>action</th>
          </tr>
        </thead>
        <tbody>
          {data.map((category, i) => (
            <tr key={category._id}>
              <td>{i + 1}</td>

              <td>
                <img
                  src={`${UPLOAD_URL}/${category.icon}`}
                  width="50"
                  height="50"
                />
              </td>
              <td>
                <img
                  src={`${UPLOAD_URL}/${category.image}`}
                  width="50"
                  height="50"
                />
              </td>
              <td>{category.title}</td>
              <td>
                <button style={{backgroundColor:"green"}}
                  onClick={() => {
                    updateCategory(category._id);
                  }}
                >
                  update <i className="refresh"></i> 
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Content;
