import axios from "axios";
import React, { useState } from 'react'
import Link from 'next/link';

import { API_URL } from "../../utils/consts";
const Content = ({ data, token, getAllChoices, setLoading,isAddModalOpen, isModalOpen, setIsAddModalOpen}) => {

	const handleCloseAddModal = () => {
		setIsAddModalOpen(false);
	};
    const delet = (id) =>{
		
        const config = {
			method: "DELETE",
			url: `${API_URL}/admins/bookchoices/${id}`,
			 headers: {
				Authorization: `Bearer ${token}`,
			},
			};
            setLoading(true)
            axios( config ).then((res) => {
                    
                    getAllChoices()
                } 
               ).catch((err) => {
                   console.error("err", err);
               }).finally(()=>{
                   setLoading(false)
               })
               }	

           return (
		<>
		
		<div className='container-table'>
			<table>
				<thead>
					<tr>
						<th>#</th>
                        <th>Book</th>
						<th>Model</th>
						<th>Delete</th>
				    </tr>
				</thead>
				<tbody>
					{data.map((choice, i) => (
						 <>
						 
                         <tr key={choice._id}>
						 
							<td>{i + 1}</td>
							<td>{choice.book.title}</td>
							<td>{choice.onModel}</td>
							<td>
								<button onClick={()=>{ delet(choice._id)}} >Delete</button>
							</td>
							
				        </tr>
						
                        <style jsx>{`
						button {
							margin-right: 2rem;
							min-width: 85px;
							padding:0.5rem 0.5rem;
							border: none;
							height:30px;
							color: white;
							border-radius: 5px;
							dispaly:flex;
							align-items:center;
							justify-content:center;
							font-size:13px;
							cursor: pointer;
							font-weight: 400;
                            background-color:red;
						  }
						  button p{
							margin-bottom:8px;
						  }
					`}</style>
						</>
					))}
				</tbody>
			</table>
		</div>
		</>
	);
};

export default Content;
