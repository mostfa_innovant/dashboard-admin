import axios from "axios";
import React, { useState } from "react";
import Link from "next/link";
import { API_URL } from "../../utils/consts";
import { UPLOAD_URL } from "../../utils/consts";
const Content = ({ data, token, getAllPBooks, setLoading }) => {
  const addChoice = (id) => {
    let body = { bookId: `${id}` };
    const config = {
      method: "POST",
      url: `${API_URL}/admins/bookchoices`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: body,
    };
    setLoading(true);
    axios(config)
      .then((res) => {
        getAllPBooks();
      })
      .catch((err) => {
        console.error("err", err);
      })
      .finally(() => {
        setLoading(false);
      });
    alert("added");
  };

  const updateStatus = (e, id) => {
    e.preventDefault();
    if (e.target.value == "add") {
      addChoice(id);
      return;
    }
    let body = { hiden: { hide: `${e.target.value}`, message: "text" } };

    const config = {
      method: "PUT",
      url: `${API_URL}/admins/physicalbooks/${id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: body,
    };

    setLoading(true);
    axios(config)
      .then((res) => {
        console.log(res);
        getAllPBooks();
      })
      .catch((err) => {
        console.error("err", err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <div className="container-table">
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>title</th>
            <th>Hidden</th>
            <th>cover</th>
            <th>Settings</th>
          </tr>
        </thead>
        <tbody>
          {data.map((book, i) => (
            <>
              <tr key={book._id}>
                <td>{i + 1}</td>
                <td>
                  <Link href={`/pbooks/${book._id}`}>{book.title}</Link>
                </td>

                <td>{book.hiden.hide == 0 ? (
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <div className="roundapp"></div>
                      <span>unhidden</span>{" "}
                    </div>
                  ) : (
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <div className="roundrejec"></div>
                      <span>hidden</span>{" "}
                    </div>
                  )}</td>

                <td>
                  <img
                    src={`${UPLOAD_URL}/${book.cover}`}
                    width="50"
                    height="50"
                  />
                </td>
                <td>
                  <div className="box">
                    <select
                      className="options"
                      onChange={(e) => {
                        updateStatus(e, book._id);
                      }}
                    >
                      <option selected disabled>
                        Action
                      </option>
                      <option value="true">hidden</option>
                      <option value="false">unhidden</option>
                      <option value="add">add to choices</option>
                    </select>
                  </div>
                </td>
              </tr>
              <style jsx>{`
                button {
                  margin-right: 2rem;
                  min-width: 85px;
                  padding: 0.5rem 0.5rem;
                  border: none;
                  height: 30px;
                  color: white;
                  border-radius: 5px;
                  dispaly: flex;
                  align-items: center;
                  justify-content: center;
                  font-size: 13px;
                  cursor: pointer;
                  font-weight: 400;
                  margin-top: 10px;
                }
                td {
                  position: relative;
                }

                .box {
                  height: 40px;
                  margin-top: 20px;
                }
                .roundapp {
                  height: 10px;
                  width: 10px;
                  border-radius: 50%;
                  background-color: green;
                  margin-right: 6px;
                }

                .roundrejec {
                  height: 10px;
                  width: 10px;
                  border-radius: 50%;
                  background-color: #fc4b6c;
                  margin-right: 6px;
                }
                .box select {
                  height: 40px;
                  font-size: 14px;
                  position: absolute;
                  top: 20px;
                  left: 0;
                }

                .box:hover::before {
                  color: white;
                  background-color: #eee;
                }

                .box select option {
                  padding: 8px 16px;
                  background-color: white;
                  font-weight: 400;
                  font-size: 14px;
                  font-family: Ppopins;
                  color: #67757c;
                }
              `}</style>
            </>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Content;
