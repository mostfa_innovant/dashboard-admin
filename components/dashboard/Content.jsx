import { CardAnalytics } from "../widgets";

const Content = () => {
	return (
		<>
			<div className='grid'>
				
				<CardAnalytics title='Users' value={0} icon='fas fa-users' color='#e35b5a' link='/users' />
				<CardAnalytics title='Partners' value={0} icon='fas fa-users' color='' link='/partners' />
				
				
			</div>

			<style jsx>{`
				.grid {
					display: grid;
					grid-template-columns: repeat(4, 1fr);
					gap: 30px;
				}

				@media only screen and (max-width: 767px) {
					.grid {
						grid-template-columns: repeat(2, 1fr);
						gap: 10px;
					}
				}
			`}</style>
		</>
	);
};

export default Content;
