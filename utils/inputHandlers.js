export const markInputAsFalse = (elmId, msg) => {
	const elm = document.getElementById(elmId);

	if (elm) {
		if (elmId !== "form" && !elmId.startsWith("not-input")) {
			elm.focus();

			elm.style.border = "2px solid #ff0000";
		}

		const oldErrorMsg = document.getElementById(`error-${elmId}`);

		if (oldErrorMsg) {
			oldErrorMsg.remove();
		}

		let errorMsg = document.createElement("p");
		errorMsg.setAttribute("id", `error-${elmId}`);
		errorMsg.innerHTML = msg;
		elm.after(errorMsg);
	}
}

export const markInputAsTrue = (elmId) => {
	const elm = document.getElementById(elmId);

	if (elm) {
		if (elmId !== "form" && !elmId.startsWith("not-input")) {
			elm.focus();
			elm.style.border = "1px solid #242424";
		}

		const errorMsg = document.getElementById(`error-${elmId}`);

		if (errorMsg) {
			errorMsg.remove();
		}

		const formErrorMsg = document.getElementById('error-form')

		if (formErrorMsg) {
			formErrorMsg.remove();
		}
	}
};
