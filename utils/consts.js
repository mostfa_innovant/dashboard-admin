export const SERVER_URL = "https://api.thejourney.space";
export const API_URL = "https://api.thejourney.space/api/v1";
export const UPLOAD_URL = SERVER_URL + "/uploads";
export const EMAIL_FORMAT = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
