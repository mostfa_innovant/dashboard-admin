import { API_URL } from "./consts";

const serverSideGetter = async (url, token) => {
    const res = await fetch(`${API_URL}/${url}`, {
        method: 'GET',
        headers: new Headers({
            'Authorization': `Bearer ${token}`,
        }),
    });

    if (res.status === 200) {
        return await res.json();
    }

    return;
}

export default serverSideGetter;