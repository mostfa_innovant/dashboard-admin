const ROUTES =  [
    
    {
        title: "Users",
        link: "users",
        icon: "fas fa-users"
    },
     {
        title: "Partners",
        link: "partners",
        icon: "fas fa-users"
    },
    {
        title: "Categories",
        link: "categories",
        icon: "fa fa-list-alt"
    },
    {
        title: "Physical Books",
        link: "pbooks",
        icon: "fas fa-book"
    },
    {
        title: "eBooks",
        link: "ebooks",
        icon: "fas fa-book"
    },
    {
        title: "Reclamations",
        link: "reclamations",          
        icon: "fa fa-comments"
    },
    {
        title: "BookChoices",
        link: "bookchoices",          
        icon: "fas fa-book"
    },
]

export default ROUTES;