import { useEffect, useState } from "react";
import axios from "axios";
import Layout from "../components/layout";
import { Container, PageTitle, PageHeader } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";
import Content from "../components/PhysicalBooks/Content";
import { API_URL } from "../utils/consts";
import Pbook from "../components/filter/Pbook";
const Index = ({ token }) => {
  const [pbooks, setPbooks] = useState([]);
  const [filtered, setFiltered] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);

  useEffect(() => {
    getAllPBooks();
  }, [!loading]);

  const getAllPBooks = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/admins/physicalbooks`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios(config)
      .then(({ status, data }) => {
        if (status === 200) {
          setPbooks(data.data);

          setFiltered(data.data);
        }
      })
      .catch((err) => {
        console.log("hello");
        console.error("err", err);
      });
  };

  const handleCloseAddModal = () => {
    setIsAddModalOpen(false);
  };

  return (
    <Layout activeSection={3}>
      <PageTitle title="Physical Books" />
      <Pbook
        pbooks={pbooks}
        setFiltered={setFiltered}
        filtered={filtered}
      ></Pbook>
      <Container>
        <Content
          token={token}
          data={filtered}
          getAllPBooks={getAllPBooks}
          setLoading={setLoading}
          loading={loading}
        />
      </Container>
    </Layout>
  );
};

export const getServerSideProps = requirePageAuth;

export default Index;
