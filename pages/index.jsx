import nextCookie from "next-cookies";
import Image from "next/image";
import { Center, Spinner } from "../components/widgets";

const Index = () => {
  return (
    <>
      <main>
        <Center>
          <div className="logo">
            <Image
              src="/img/logo.png"
              alt="Wh International"
              layout="fill"
              objectFit="contain"
            />
          </div>

          <Spinner color="#E1CB1F" size={30} />
        </Center>
      </main>

      <style jsx>{`
        main {
          width: 100vw;
          height: 100vh;
        }

        .logo {
          width: 196px;
          height: 160px;
          margin-bottom: 15px;
          position: relative;
        }
      `}</style>
    </>
  );
};

export const getServerSideProps = (ctx) => {
  const { token, role } = nextCookie(ctx);

  if (token) {
    return {
      redirect: {
        destination: "/dashboard",
        permanent: false,
      },
    };
  }

  return {
    redirect: {
      destination: "/login",
      permanent: false,
    },
  };
};

export default Index;
