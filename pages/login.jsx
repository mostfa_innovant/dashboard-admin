import { useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import cookie from "js-cookie";
import nextCookie from "next-cookies";
import { Center } from "../components/widgets";
import { EMAIL_FORMAT } from "../utils/consts";
import { markInputAsFalse, markInputAsTrue } from "../utils/inputHandlers";
import { API_URL } from "../utils/consts";

const Index = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errMsg, setErrMsg] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    setErrMsg("");

    if (isValid()) {
      const url = `${API_URL}/admins/login`;
      const body = { email, password };

      axios
        .post(url, body)
        .then(({ status, data }) => {
          if (status === 200) {
            cookie.set("token", data.token, { expires: 1 });
            cookie.set("email", data.email, { expires: 1 });

            router.push(`/users`);
          }
        })
        .catch((err) => {
          if (err.response) {
            const { status, data } = err.response;

            if (status === 400 || status === 401) {
              setErrMsg(data.error);
            }
          }
        });
    }
  };

  const handleInputChange = (e, fn) => {
    fn(e.target.value);
    markInputAsTrue(e.target.id);
    setErrMsg("");
  };

  const isValid = () => {
    if (!email) {
      markInputAsFalse("email", "Please enter your email");
      return false;
    }
    if (!email.match(EMAIL_FORMAT)) {
      markInputAsFalse("email", "The entered email is not valid");
      return false;
    }
    if (!password) {
      markInputAsFalse("password", "Please enter your password");
      return false;
    }
    if (password.length < 6) {
      markInputAsFalse(
        "password",
        "Password is shorter than the minimum allowed length (6)"
      );
      return false;
    }

    return true;
  };

  return (
    <>
      <main>
        <Center>
          <div className="logo">
            <img src="/img/logo.svg" alt="" style={{ height: "90px" }} />
          </div>

          <form onSubmit={handleSubmit}>
            <input
              id="email"
              type="email"
              placeholder="Email"
              autoComplete="username"
              value={email}
              onChange={(e) => handleInputChange(e, setEmail)}
            />
            <input
              id="password"
              type="password"
              placeholder="Password"
              autoComplete="current-password"
              value={password}
              onChange={(e) => handleInputChange(e, setPassword)}
            />

            <button type="submit">Login</button>

            {errMsg && <p id="error-form-1">{errMsg}</p>}
          </form>
        </Center>
      </main>

      <style jsx>{`
        main {
          width: 100vw;
          height: 100vh;
        }

        .logo {
          width: 123px;
          height: 100px;
          margin-bottom: 15px;
          position: relative;
        }

        h1 {
          color: #fff;
        }

        form {
          margin-top: 30px;
        }

        input {
          width: 320px;
        }

        input:nth-child(2) {
          margin-top: 10px;
        }

        button {
          width: 100%;
          margin-top: 30px;
        }
      `}</style>
    </>
  );
};

export const getServerSideProps = (ctx) => {
  const { token } = nextCookie(ctx);

  if (token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return { props: {} };
};

export default Index;
