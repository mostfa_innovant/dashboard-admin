import { useEffect, useState } from "react";
import axios from "axios";
import Layout from "../components/layout";
import { Container, PageTitle, PageHeader } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";
import { API_URL } from "../utils/consts";
import Content from "../components/choices/Content";

const Index = ({ token }) => {
  const [choices, setChoices] = useState([]);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  const getAllChoices = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/bookchoices`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios(config)
      .then(({ status, data }) => {
        if (status === 200) {
          setChoices(data.data);
          console.log(data);
        }
      })
      .catch((err) => {
        console.error("err", err);
      });
  };

  const handleCloseAddModal = () => {
    setIsAddModalOpen(false);
  };
  useEffect(() => {
    getAllChoices();
  }, []);

  return (
    <Layout activeSection={6}>
      <PageTitle total={choices.length} />
      <Container>
        <Content
          data={choices}
          token={token}
          getAllChoices={getAllChoices}
          setLoading={setLoading}
          loading={loading}
        />
      </Container>
    </Layout>
  );
};

export const getServerSideProps = requirePageAuth;

export default Index;
